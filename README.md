# Hello

## The main goal of this project

This is the starter project for the final assessment project for Udacity's Redux course where you will build a content and comment web app. Users will be able to post content to predefined categories, comment on their posts and other users' posts, and vote on posts and comments. Users will also be able to edit and delete posts and comments.

This repository includes the code for the backend API Server that you'll use to develop and interact with the front-end portion of the project.

## The design references

### http://bettermotherfuckingwebsite.com/

Ok, the name of this website is not good specially if you are sending it to the place where you want to work but ...

Was my first visual reference for this project. If you spend some time reading the message that the website tries to give it get's really funny.
The base is, basic design, one size fits all (screens) and 7 lines of CSS.

On the first page of the project you will see the visual references.

### https://ember-wp.ecko.me/using-redis-as-a-caching-layer/

This was the main design reference that i used to make the article page.

Big image first, content on the bottom, you can't go wrong with this one.

## Everything is there

On the first page you can: 

- Change between categories and see the diferent feeds
- Like a Post
- Create a Post

After clicking the "Read more" button or the comments button, you will jump to the articles page where you can:

- Add a comment for that Post
- View the full picture if it does not fit the screen
- Like any comment
- Edit/Delete the post (If you own it)
- Edit/Delete the comment (If you own it)

The header is present on every page and it is where you can click the BI logo and go to the main page or change categories.

There is also a "Post Editor" and "Comment editor" that you will discover if you click Edit on your comments or posts.

## Don't mind the design

Ok, i followed some nice visual references but i am no designer ... but hey, the project is working.

# Developers stuff now

## Remember!!!

Crashes can happen ... validating props or API response was not my main priority.

I really wanted to have everything working within 8 hours of coding and that was my main goal.

Everything should go smoothly but if it crashes, just refresh the page and it should be ok!

## The big problem

First of all, i started by adding all the Reducer actions, Reducers, and API calls ... and it took me a while, it's a long copy/paste process.

For every call i have:

4 actions:
  - The main request to be used in the front end.
  - The action when you start making the request
  - The action when it fails
  - The action when it succeeds

3 Reducers
  - The reducer when you start making the request
  - The reducer when it fails
  - The reducer when it succeeds

1 Fetch
  - The main API fetch that communicates with the server

And everything was fine untill i needed to use a POST action, and for some reason (maybe this course was too old) there were some missing settings for Express to work.

I was sending a valid body to the request and it was not reaching the server, i changed the 'like' method for the post too use the url parameters instead of the body, and it worked fine but it was just a small moment of glory, i need to update posts and comments that can contain big text and the query parameters could not be used in this case.

After digging in the express github and stackoverflow i added some properties that at the end made it work.

They were: 
`
app.use(express.static('public'))
app.use(cors())
app.use(bodyParser.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
`
After that, express was reading the request.body again and i could finally continue on the project.

## Packages Used

    jquery - Used Hquery just to make a scroll to top, nothing really heavy
    lodash - to manimulate objects
    moment - to format dates
    node-sass-chokidar - To write CSS with SASS
    npm-run-all - SASS stuff to complile the sass into the css file at build time
    react-redux - redux stuff
    react-router-dom - To work with routes
    reading-time - To calculate the reading time of wach article
    redux - redux stuff

## Start Testing

To get started Testing right away:

- Clone the repo

Now, inside the repo folder:

- Install and start the API server

  - `cd api-server`
  - `npm install`
  - `node server`

 In another terminal window and again, back to the repo folder:

  - `cd frontend`
  - `npm install`
  - `npm start`

  If you did everything right and if i did not miss a step, a browser window should open anytime soon with the project.

  Usually the server will be listening on port 3001 and the front-end will be running on port 3000:

  That way, if the browser does not open you can access the fron-end in: http://localhost:3000/

## Things to improve

There are loads of thing to improve. It's not a perfect and beautiful project but it should work (at least works on my computer :) programmers jokes)

There are lots of `<p>` that should be `<a>` loads of `onClick` that should be `<Link>` but you know, time runs fast and fingers type faster :)

Mobile: It should work on mobile without any problems but was not optimized for mobile

I am loading the Header.js class more than one time wich means that i should've used a '<Layout>' to make it work one time and for every screen but i did not implement that and when i remebered it was already out of the 8 hours window for this project.

## That's all

I really hope that you guys like the project, did it in about 5 days using any spare time i could. Was really fun, it was actually!!!!
