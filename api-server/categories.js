const clone = require('clone')
const config = require('./config')

let db = {}

const defaultData = {
  categories: [
    {
      name: 'all',
      path: 'all',
      color: '#27ae60',
    },
    {
      name: 'business',
      path: 'business',
      color: '#d35400',
    },
    {
      name: 'arts',
      path: 'arts',
      color: '#8e44ad',
    },
    {
      name: 'world',
      path: 'world',
      color: '#2980b9',
    },
    {
      name: 'in pictures',
      path: 'inpictures',
      color: '#F79F1F',
    },
  ]
}

function getData(token) {
  //Each token has it's own copy of the DB. The token in this case is like an app id.
  let data = db[token]
  //This populates the default user data if there isn't any in the db.
  if (data == null) {
    data = db[token] = clone(defaultData)
  }
  return data
}

function getAll(token) {
  return new Promise((res) => {
    res(getData(token))
  })
}

module.exports = {
  getAll
}
