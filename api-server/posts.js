const clone = require('clone')

let db = {}

let defaultData = {
  "8xf0y6ziyjabvozdd253nd": {
    id: '8xf0y6ziyjabvozdd253nd',
    timestamp: 468022598,
    title: 'BMW, Daimler and Volkswagen face EU diesel emissions probe',
    secondTitle: 'German carmakers BMW, Daimler and Volkswagen are to face an EU inquiry for allegedly conspiring to restrict diesel emissions treatment system',
    body: "The European Commission said it was investigating whether they agreed to limit the development of systems to reduce harmful emissions. It said that if proven, this could mean that consumers had been denied the chance to buy less polluting cars. The firms were raided in 2017 as part of the Commission's earlier inquiries. The Commission said its in-depth investigation was intended to assess whether the carmakers colluded, in breach of EU anti-trust rules, to avoid competing on technology to clean up petrol and diesel car emissions. It said it was focusing on information indicating that the companies, including VW-owned Audi and Porsche, had met to discuss the development and deployment of emissions technology.",
    secondBody: "All three companies issued statements saying they were co-operating fully with the Commission and would not comment further on the proceedings. Daimler pointed out that the proceedings related exclusively to Europe and that there had been no price-fixing allegations. BMW said it was wholeheartedly committed to the principles of market economics and fair competition. The Commission said it had no indications that the carmakers co-ordinated with each other on the use of illegal emissions-cheating defeat devices. Volkswagen admitted in 2015 to using defeat device software in the US to cheat diesel engine emissions tests, plunging the company into scandal. Since then, emissions irregularities have surfaced at several major carmakers, although none were found to be as serious as at Volkswagen.",
    author: 'Arthur Mayer',
    avatar: 'http://i.pravatar.cc/300?img=11',
    image: 'https://cld.pt/dl/download/dc9c16e5-4d4c-4e1e-8e31-6edb764639d9/business2.jpg',
    category: 'business',
    categoryName: 'business',
    voteScore: 6,
    deleted: false,
    commentCount: 2
  },
  "6ni6ok3ym7mf1p33lnez": {
    id: '6ni6ok3ym7mf1p33lnez',
    timestamp: 344635781,
    title: 'People thought we were interns but we were in charge',
    secondTitle: "The BBC's weekly The Boss series profiles business leaders from around the world. This week we speak to Jamie Beaton and Sharndre Kushor, founders of online tutoring company Crimson Education.",
    body: "When Jamie Beaton and Sharndre Kushor were seeking investors for their business, the fact that they were teenagers caused some confusion. We'd sit down in these boardrooms, and everyone would be triple our age, lots of white hair and beige, says Jamie, now 23. We'd turn up and they'd think we were the assistants or the interns. Thankfully for all concerned, boyfriend and girlfriend Jamie and Sharndre were not there on work experience, or to pour the coffees. The New Zealand couple are instead the founders and bosses of Crimson Education, which they launched when they were both 18. Aimed at high school students around the world, it links them with tutors and mentors to help them get into the best universities in the US and the UK.",
    secondBody: "When the couple started the business in Auckland in 2013 they were finishing their final year at school. With no money to advertise, they instead reached out to friends and teachers, and put up notices on Facebook, to secure their first students and tutors. A year later Crimson gained its first $1m (£770,000) in funding, and it started to grow steadily. Fast forward to today, and the company has reportedly secured a total of $37m in investment, which values it at $160m. And it claims that more than 20,000 students around the world now use its network of 2,400 academics and career advisers. Jamie and Sharndre started dating when they were two of 18 Kiwi students chosen to attend a Model United Nations event in The Hague, Netherlands. Model UN is a global scheme in which children and young adults learn about diplomacy and international relations by role playing as delegates to the United Nations. Our first date was at a Starbucks in a train station in Germany, while on the trip, says Sharndre, also now 23. She says she was impressed by Jamie's do-or-die focus.",
    author: 'Mary Olson',
    avatar: 'http://i.pravatar.cc/300?img=16',
    image: 'https://cld.pt/dl/download/f29013a8-d28e-4f3b-8e48-994341e2e508/business1.jpg',
    category: 'business',
    categoryName: 'business',
    voteScore: -5,
    deleted: false,
    commentCount: 0
  },
  "qalavyuo4czhhiynq3ao": {
    id: 'qalavyuo4czhhiynq3ao',
    timestamp: 549403671,
    title: 'Can the city of pizza reinvent itself as a tech capital?',
    secondTitle: "As tech giants Apple and Cisco set up academies in Naples, the city's tech entrepreneurs are hoping new investment will revitalise southern Italy's failing economy.",
    body: "Naples is a city with a reputation: it has pizza, devil-may-care drivers, and mafia clans known as the Camorra. But in the past few years this southern Italian city has also been fostering a growing community of tech start-ups and app creators. The hope is it will change not just Naples' reputation, but also its fortunes and so reverse a brain drain that's seen many of the city's young graduates leave to find jobs in the more prosperous north of Italy, or even abroad. Naples, and its region, Campania, is part of the Mezzogiorno (southern Italy and Sicily) which lags behind the rest of the country in terms of economic growth. Here the jobless rate was 22.2% in the first quarter of this year, almost double the national average.",
    secondBody: "But that hasn't put off a growing list of Neapolitan tech firms - influencer marketing company Buzzoole and the agricultural tech start-up Evja to name but two. Evja makes sensors that are placed in fields and greenhouses to transmit real-time indicators of growing conditions. We want to prove it's possible to do business here, says Evja founder Paolo Iasevoli, pointing out that his company is now selling overseas and soon launching in south American markets. What has really changed the game for Naples' tech scene is Apple's recent arrival in the city. In 2015, Apple opened an academy in Naples, in conjunction with University of Naples Federico II, where students spend a year training to be developers, coders, app creators and start-up entrepreneurs.",
    author: 'John Paolo',
    avatar: 'http://i.pravatar.cc/300?img=55',
    image: 'http://walldiskpaper.com/wp-content/uploads/2015/01/Napoli-Italy-Wallpaper-Landscape-Free-Downloads.jpg',
    category: 'business',
    categoryName: 'business',
    voteScore: 10,
    deleted: false,
    commentCount: 0
  },
  "le6fjt8vmdolx6rn4tz3": {
    id: 'le6fjt8vmdolx6rn4tz3',
    timestamp: 998367179,
    title: 'Ferrari to launch 15 new models by 2022',
    secondTitle: "Italian sports carmaker Ferrari has said it will launch 15 new models by 2022, as it embarks on a new five-year strategy under a new boss.",
    body: "The company also intends to launch its first SUV, Purosangue, at the end of the five-year period. Chief executive Louis Camilleri said his plan was ambitious but do-able, based on a detailed framework. Mr Camilleri took the helm at Ferrari in July, following the death of Sergio Marchionne. Ferrari now wants to achieve adjusted core earnings of €1.8bn-€2bn (£1.6bn-£1.8bn) by 2022. The aim is to develop new vehicles and move towards hybrid petrol-electric models, which will make up 60% of its range by the end of the five-year plan. Chief marketing officer Enrico Galliera said the new models would come with a significant increase in the average price.",
    secondBody: "In a touch of glamour, Ferrari is seeking to capitalise on its Formula 1 success with a new limited-edition open-top racing-style supercar. The Monza will come in single-seater and two-seater versions as part of a new range dubbed Icona. Ferrari will build fewer than 500 of the two models combined and all have already been snapped up. The Monza is intended as a successor to the classic Barchetta, a racing car made by the firm in the 1940s and 1950s. It will have an 810-horsepower V-12 engine and a lightweight carbon-fibre body. The car will take just three seconds to reach 100km/h. The Monza was first shown to investors and customers late on Monday. It will be on display to the public at the Paris motor show next month, where the company will also reveal pricing details. Industry experts say supercars of this kind typically cost more than $1m.",
    author: 'Dany Pierre',
    avatar: 'http://i.pravatar.cc/300?img=5',
    image: 'https://wallpaperstudio10.com/static/wpdb/wallpapers/1920x1080/177348.jpg',
    category: 'business',
    categoryName: 'business',
    voteScore: 7,
    deleted: false,
    commentCount: 0
  },
  "wgmvmb7h0vew84drs55e": {
    id: 'wgmvmb7h0vew84drs55e',
    timestamp: 733482172,
    title: "Captain Marvel: Why Brie Larson's Carol Danvers is a Marvel game-changer",
    secondTitle: "Captain America and co were left in a tricky position at the end of Avengers: Infinity War, but help may be at hand with the arrival of Captain Marvel.",
    body: "The first trailer for Brie Larson's Marvel movie has premiered ahead of its 2019 release. Marvel Studios Chief Kevin Feige has described Carol Danvers as the most powerful character so far in the Marvel Cinematic Universe. So it seems there's finally someone set to go toe-to-toe with villain Thanos. The trailer sees Brie meeting Samuel L. Jackson as Nick Fury after a period of time spent away from planet earth, power up as Captain Marvel and punch a smiling old lady in the face (who is almost certainly an alien in disguise - we hope). The film is set in the nineties - in case the extras in retro clothes, a Blockbuster video store and pagers didn't give that away.",
    secondBody: "The movie is the first Marvel film to have a female star as its lead in a decade of building its Marvel Cinematic Universe. The closest we've come before now is when Evangeline Lilly's character The Wasp appeared in the title of Ant Man sequel, Ant Man and The Wasp. That means a lot to viewers like Aida Latifi, who's 18, lives in Iran and started watching Sam Raimi's Spider-Man movies after school when she was younger. Before I saw the Avengers I didn't even know that female superheroes even existed, she tells Newsbeat. It didn't even cross my mind. As weird as it might be, seeing Black Widow just boosted my confidence as a child.",
    author: 'João Oliveira',
    avatar: 'http://i.pravatar.cc/300?img=33',
    image: 'http://longwallpapers.com/Desktop-Wallpaper/marvel-wallpapers-desktop-background-For-Desktop-Wallpaper.jpg',
    category: 'arts',
    categoryName: 'arts',
    voteScore: 1,
    deleted: false,
    commentCount: 0
  },
  "7sauikzkzhlrmcdeh8ky": {
    id: '7sauikzkzhlrmcdeh8ky',
    timestamp: 248272150,
    title: 'Bodyguard: David Budd gives out his number, viewers try to call it',
    secondTitle: "There were plenty of twists and turns in the latest episode of BBC drama Bodyguard - but one had some viewers reaching for their mobile phones.",
    body: "At one point, Richard Madden's character, David Budd, gave out his mobile number. It was too tempting for a number of viewers, who couldn't resist calling it, to see if it got through to him. They were left disappointed and there are 20,000 reasons why. Or rather 20,000 combinations. That's the amount of fake numbers set aside by broadcasting regulator Ofcom, for use in TV, radio and film. These include numbers for different geographic areas around the UK as well as mobile phone numbers.",
    secondBody: "Harry Rippon, communications manager for Ofcom, says this is to save anyone from having their phone number featured on TV - because usually, the first thing people do is call it. I think it happens quite a lot - if a number is onscreen, there's always the chance someone could call it, he tells Newsbeat. There's always someone who thinks: 'What if I call this number? As far as I'm aware, we've always had these numbers for this very reason. That's possibly what happened after Bodyguard broadcast PC Budd's fake number. Some fans joked about noting it down.",
    author: 'David Still',
    avatar: 'http://i.pravatar.cc/300?img=51',
    image: 'https://zeusprotection.co.za/wp-content/uploads/2015/05/body-guard-Security-Guards-vip-protection-services-bodyguard-services-south-africa-1024x498.png',
    category: 'arts',
    categoryName: 'arts',
    voteScore: 13,
    deleted: false,
    commentCount: 0
  },
  "nn0r7mgcy6guhjxg7e6j": {
    id: 'nn0r7mgcy6guhjxg7e6j',
    timestamp: 404203561,
    title: 'Toronto Film Festival: Breakfast Club recreated in starry script reading',
    secondTitle: "Jesse Eisenberg, Christina Hendricks and others have joined Jason Reitman for a live reading of The Breakfast Club at the Toronto Film Festival.",
    body: "Breaking Bad's Aaron Paul and Britain's Richard E Grant also took part in the tribute to John Hughes' teen classic. Molly Ringwald, Emilio Estevez and Judd Nelson starred in the 1985 original, which told of five high school students spending a Saturday in detention. Hughes, who died in 2009 aged 59, was represented by his son James. Reitman, himself the son of Ghostbusters director Ivan, introduced him from the stage as a fellow member of the nepotism club",
    secondBody: "Highlights of the evening included Eisenberg recreating Nelson's famous fist salute and Hendricks emulating the scene where Ringwald applies lipstick without using her hands. Boogie Nights, The Princess Bride and American Beauty are among the films to receive the live read treatment at previous festivals. Hughes' film continues to be held in great regard 33 years on, as evidenced by a recent photo showing the cast of the new Wonder Woman film recreating its iconic poster.",
    author: 'Neil Smith',
    avatar: 'http://i.pravatar.cc/300?img=52',
    image: 'http://getwallpapers.com/wallpaper/full/1/2/8/449112.jpg',
    category: 'arts',
    categoryName: 'arts',
    voteScore: 22,
    deleted: false,
    commentCount: 0
  },
  "c1pusemrexk3anl097k7": {
    id: 'c1pusemrexk3anl097k7',
    timestamp: 631790973,
    title: 'Stranger Things star officiates fan wedding in costume',
    secondTitle: "Two Stranger Things fans were treated to a wedding in the Upside Down, thanks to star David Harbour.",
    body: "The actor, who plays chief of police Jim Hopper in the Netflix show, married the fans after they challenged him on Twitter. He said if they got a certain number of retweets, he would get ordained and marry them. David kept his word, officiating the ceremony in full costume over the weekend. He tweeted a picture of himself and the congregation, writing so me and some fun folks in Springfield, Illinois made good on our promise we made all those months ago. The bride got in contact with the actor back in January and he said he would be on board, but set out a number of conditions.",
    secondBody: "These included making sure the wedding didn't clash with the filming schedule of Stranger Thing's third season, that he would be allowed to read a love letter at the wedding and get the first slice of the cake after it was cut. He also put in another condition, that the tweet had to be retweeted over 125,000 times. This was no problem for the couple and David kept his word. The act of goodwill isn't David's first either - in October last year, he posed for a student's school photo after she contacted him on Twitter. He also made a few outlandish requests alongside demanding 25,000 retweets, including wearing a jumper with her school's logo and holding a trombone.",
    author: 'Paula Allen',
    avatar: 'http://i.pravatar.cc/300?img=20',
    image: 'https://cld.pt/dl/download/bf8dbeb3-6d9b-46d6-954a-f570fb54e285/arts4.jpg',
    category: 'arts',
    categoryName: 'arts',
    voteScore: 9,
    deleted: false,
    commentCount: 0
  },
  "dthyiflynsl7w87jz6oq": {
    id: 'dthyiflynsl7w87jz6oq',
    timestamp: 189322316,
    title: 'Russian aircrew deaths: Putin and Netanyahu defuse tension',
    secondTitle: "The leaders of Russia and Israel have sought to defuse tension after a Russian plane was shot down by Syrian forces amid an Israeli air raid.",
    body: "Mr Putin and Mr Netanyahu spoke by telephone on Monday, shortly after the Russian president had made his conciliatory statement in Moscow. Mr Putin said at a media briefing: It looks most likely in this case that it was a chain of tragic chance events, because an Israeli aircraft did not shoot down our aircraft. But, without any doubt we need to seriously get the bottom of what happened. Mr Netanyahu addressed that issue in the telephone call, offering to provide all necessary information in the investigation of the incident, as well as expressing regret over the fatalities.",
    secondBody: "In the call, Mr Putin urged Mr Netanyahu not to let such a situation happen in the future and told him Israeli air operations breached Syrian sovereignty, Russian media said. Russia's Defence Minister Sergei Shoigu had earlier said that Moscow reserves the right to take further steps in response. But Mr Putin said the retaliatory measures would be aimed first and foremost at further ensuring the safety of our military personnel. The BBC's defence and diplomatic correspondent, Jonathan Marcus, says the crisis will almost certainly blow over as Israel and Russia have a remarkably close relationship and Moscow has until now not interfered with Israeli operations.",
    author: 'Maria Mendes',
    avatar: 'http://i.pravatar.cc/300?img=21',
    image: 'https://c1.staticflickr.com/5/4440/36923959816_95c73f8b7c_b.jpg',
    category: 'world',
    categoryName: 'world',
    voteScore: 16,
    deleted: false,
    commentCount: 0
  },
  "pymunb8dgfk6taj4m7zc": {
    id: 'pymunb8dgfk6taj4m7zc',
    timestamp: 360319550,
    title: 'Lynette Dawson: No remains found in podcast-famous mystery police dig',
    secondTitle: "Australian police say they have found no remains or items of interest during a dig at the former home of a Sydney woman whose whereabouts have been a mystery since she disappeared in 1982.",
    body: "Lynette Dawson, a mother of two young children, vanished without a trace. A popular podcast on her disappearance has brought wide attention to the case. Her husband has denied murdering her, saying she left the family, perhaps for a religious group. Two inquests found she was killed by a known person. The new search at the home involved a radar examination of the grounds, including the swimming pool where some believed her remains may be buried, reports say; police also drained the property's septic tank and brought in a blood-detection dog. They used earth-drilling machinery to break up the soil between the front of the house and the pool, which was then sifted through for any traces of remains or evidence.",
    secondBody: "Last week, police said they had begun a five-day forensic search of the couple's former home on a leafy street of multi-million-dollar properties in the suburb of Bayview, on the northern beaches of the city. They later extended the search by a day, but found no new evidence. A previous search in 2000 ended prematurely because of budget constraints, The Australian newspaper reports.",
    author: 'Caty Oliver',
    avatar: 'http://i.pravatar.cc/300?img=23',
    image: 'https://cld.pt/dl/download/24237909-e9e4-488a-a62d-9af2f000e2f9/world2.jpg',
    category: 'world',
    categoryName: 'world',
    voteScore: 23,
    deleted: false,
    commentCount: 0
  },
  "s53u1d7chbygzeqgfz91": {
    id: 's53u1d7chbygzeqgfz91',
    timestamp: 100386672,
    title: 'The truth about false assault accusations by women',
    secondTitle: "Either Brett Kavanaugh or Christine Blasey Ford is lying. We don't know which one.",
    body: "According to various academic studies over the past 20 years, only 2-10% of rape accusations are fake (Prof Ford's lawyer says she believes this was attempted rape). Two to 10% is too many, but it is not a big proportion of the total. Fake rape accusations get a lot of attention. Both the Duke Lacrosse team case in 2006 and the alleged University of Virginia gang rape in 2014 were widely covered by the media. They were terrible miscarriages of justice - but they were not representative.",
    secondBody: "The idea that lots of men are going to prison because they've been falsely accused of rape isn't supported by the facts. Moreover, official figures suggest the number of rapes and sexual assaults which are never reported or prosecuted far outweighs the number of men convicted of rape because of fake accusations. Indeed it far outweighs the number of fake accusations, period. Figures from the US Bureau of Justice Statistics suggest only 35% of all sexual assaults are even reported to the police.",
    author: 'Ali Xin',
    avatar: 'http://i.pravatar.cc/300?img=25',
    image: 'https://s3.amazonaws.com/wapopartners.com/dbknews-wp/wp-content/uploads/2018/03/04202416/Female_prisoner_shackled_in_her_small_cell.jpg',
    category: 'world',
    categoryName: 'world',
    voteScore: 1,
    deleted: false,
    commentCount: 0
  },
  "c5b5aq65pf30tvg28deg": {
    id: 'c5b5aq65pf30tvg28deg',
    timestamp: 574018846,
    title: 'Chemnitz unrest: German top spy Maassen forced out',
    secondTitle: "Germany's domestic intelligence chief, Hans-Georg Maassen, has been told to quit and move to a senior post at the interior ministry.",
    body: "The government decision came amid a row over Mr Maassen's response to far-right unrest in Chemnitz, eastern Germany. Anti-migrant hunts were reported there on 26 August after a German man was killed in a brawl with migrants. Mr Maassen doubted that foreign-looking people had been hounded. Chancellor Angela Merkel was urged to sack him.",
    secondBody: "Critics said his scepticism downplayed the seriousness of far-right violence and intimidation in Chemnitz. Mr Maassen will leave the BfV spy service and become a state secretary in the interior ministry. German media report that he will actually move to a higher pay grade. It is not yet clear who will replace him.",
    author: 'Ana Boice',
    avatar: 'http://i.pravatar.cc/300?img=26',
    image: 'https://wallpaper-house.com/data/out/7/wallpaper2you_201750.jpg',
    category: 'world',
    categoryName: 'world',
    voteScore: 1,
    deleted: false,
    commentCount: 0
  },
  "dc4u572g4qfi5jws0udg": {
    id: 'dc4u572g4qfi5jws0udg',
    timestamp: 492228611,
    title: 'Chicago: Three artists challenging African-American stereotypes',
    secondTitle: "Three photographers are exhibiting work that confronts the way African-Americans are often perceived in art, the workplace, and through their physical appearance.",
    body: "Alanna Airitam addresses the absence of black people in the history of Western art, with rare appearances showing dark-skinned people represented in paintings and films as domestic workers, slaves or barbarians. In her series The Golden Age, the artist invited African-Americans to pose in the style of classic Dutch portraiture, to celebrate black identity and highlight the racial divide seen in art history.",
    secondBody: "Airitam said: When I see the beauty and power in the eyes of the people in the portraits, it immediately counters all the negative stereotypes and narratives we receive on a daily basis. It is (and always will be) especially important for me to combat the barrage of dehumanising messages black people face through media with messages that clearly state that we are beautiful, powerful, valuable, worthy human beings and we're here to stay.",
    author: 'Julien Field',
    avatar: 'http://i.pravatar.cc/300?img=67',
    image: 'https://cld.pt/dl/download/fb507597-7bd0-492d-8d67-b7943fe5dbd4/inpictures1.jpg',
    category: 'inpictures',
    categoryName: 'in pictures',
    voteScore: 2,
    deleted: false,
    commentCount: 0
  },
  "si4s14ipjfuuc5v0am44": {
    id: 'si4s14ipjfuuc5v0am44',
    timestamp: 737611491,
    title: "Kofi Annan's funeral: World leaders bid farewell to ex-UN chief",
    secondTitle: "World leaders and royalty have paid their respects to one of Africa's most famous diplomats, Kofi Annan, at his funeral in his home country of Ghana.",
    body: "It is the climax of three days of mourning which saw thousands of Ghanaians file past his coffin as it lay in state in the capital, Accra. Annan died on 18 August in Switzerland at the age of 80. He was UN secretary-general from 1997 to 2006, the first black African to hold the world's top diplomatic post. He was awarded the Nobel Peace Prize in 2001 for helping to revitalise the international body, during a period that coincided with the Iraq War and the HIV/Aids pandemic.",
    secondBody: "Speaking at the funeral, current UN secretary-general Antonio Guterres said Annan was an exceptional leader who saw the UN as a force for good. As we face the headwinds of our troubled and turbulent times, let us always be inspired by the legacy of Kofi Annan, Mr Guterres said. Our world needs it now more than ever, he added.",
    author: 'Donald Trimp',
    avatar: 'http://i.pravatar.cc/300?img=68',
    image: 'https://cld.pt/dl/download/dd22d56e-c0ab-4b7d-8b2e-e92e9cc787fa/inpictures2.jpg',
    category: 'inpictures',
    categoryName: 'in pictures',
    voteScore: 32,
    deleted: false,
    commentCount: 0
  }
}

function getData(token) {
  let data = db[token]
  if (data == null) {
    data = db[token] = clone(defaultData)
  }
  return data
}

function getByCategory(token, category) {
  return new Promise((res) => {
    let posts = getData(token)
    let keys = Object.keys(posts)
    let filtered_keys = keys.filter(key => posts[key].category === category && !posts[key].deleted)
    res(filtered_keys.map(key => posts[key]))
  })
}

function get(token, id) {
  return new Promise((res) => {
    const posts = getData(token)
    res(
      posts[id].deleted
        ? {}
        : posts[id]
    )
  })
}

function getAll(token) {
  return new Promise((res) => {
    const posts = getData(token)
    let keys = Object.keys(posts)
    let filtered_keys = keys.filter(key => !posts[key].deleted)
    res(filtered_keys.map(key => posts[key]))
  })
}

function add(token, post) {
  return new Promise((res) => {
    let posts = getData(token)

    let postHolder = {
      id: post.id,
      timestamp: post.timestamp,
      title: post.title,
      secondTitle: post.secondTitle,
      body: post.body,
      secondBody: post.secondBody,
      image: post.image,
      author: post.author,
      avatar: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Cnn_logo_red_background.png/200px-Cnn_logo_red_background.png',
      category: post.category,
      categoryName: post.categoryName,
      voteScore: 1,
      liked: true,
      deleted: false,
      commentCount: 0,
      owned: true
    }

    posts[post.id] = postHolder

    let dataHolder = Object.assign({}, defaultData)

    dataHolder[post.id] = postHolder

    res(posts[post.id])
  })
}

function vote(token, id, option) {
  return new Promise((res) => {
    let posts = getData(token)
    post = posts[id]

    if (option === 'upVote') {
      post.voteScore = post.voteScore + 1
      post.liked = true
    } else if (option === 'downVote') {
      post.voteScore = post.voteScore - 1
      post.liked = false
    } else {
      console.log(`posts.vote received incorrect parameter: ${option}`)
    }

    let dataHolder = Object.assign({}, defaultData)
    dataHolder[id] = post

    defaultData = dataHolder

    res(post)
  })
}

function disable(token, id) {
  return new Promise((res) => {
    let posts = getData(token)
    posts[id].deleted = true
    res(posts[id])
  })
}

function edit(token, id, post) {
  return new Promise((res) => {
    let posts = getData(token)
    for (prop in post) {
      posts[id][prop] = post[prop]
    }
    res(posts[id])
  })
}

function incrementCommentCounter(token, id, count) {
  const data = getData(token)
  if (data[id]) {
    data[id].commentCount += count
  }
}

module.exports = {
  get,
  getAll,
  getByCategory,
  add,
  vote,
  disable,
  edit,
  getAll,
  incrementCommentCounter
}
