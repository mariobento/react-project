import React from 'react'
import { render } from 'react-dom'
import './index.css'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import asyncComponent from './AsyncComponent'
import configureStore from './store/configureStore'

// Compiled css
import './App.css'

const App = asyncComponent(() => import("./App"))
const Editor = asyncComponent(() => import("./components/editor"))
const PostCreator = asyncComponent(() => import("./components/postCreator"))

const store = configureStore()

render((
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path="/" component={App} />
        <Route path="/post/:create" component={PostCreator} />
        <Route path="/article/:postId" component={App} />
        <Route path="/edit/:whatToEdit/:whatToEditId" component={Editor} />
        <Route path="/:category" component={App} />
      </Switch>
    </Router>
  </Provider>
), document.getElementById('root'))
