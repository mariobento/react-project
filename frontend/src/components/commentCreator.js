import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as mainActions from '../API/actions/mainActions'

class CommentCreator extends Component {

  constructor() {
    super()

    this.state = { creating: false, body: '' }

    this.handleChange = this.handleChange.bind(this)

  }

  handleChange(event) {
    this.setState({ ...this.state, body: event.target.value })
  }

  renderCreateButton() {

    return (
      <div className="create-button-wrapper" onClick={() => this.setState({ ...this.state, creating: true })}>
        Add Comment
      </div>
    )

  }

  makeid() {
    var text = ""
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789"

    for (var i = 0; i < 10; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length))

    return text
  }

  handleSubmit() {

    let stateHolder = Object.assign({}, this.state)

    let date = Date.now()

    this.setState({ ...this.state, creating: false, body: '' }, () => {
      this.props.actions.fetchRequestAddCommentToPost(this.makeid(), Math.floor(date / 1000), stateHolder.body, 'Mario Gaudencio', this.props.post.id)
      this.props.actions.fetchRequestPostComments(this.props.post.id)
    })

  }

  renderCommentInput() {

    return (
      <div className="form-wrapper">
        <div className="comment-author-header">
          <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Cnn_logo_red_background.png/200px-Cnn_logo_red_background.png" alt="biavatar" />
          <p>Mario Gaudencio</p>
        </div>
        <textarea value={this.state.body} onChange={this.handleChange} name="text-comment" id="text-comment" cols="12" rows="1"></textarea>
        <button onClick={() => this.handleSubmit()} type="button">Submit Comment</button>
        <button onClick={() => this.setState({ ...this.state, creating: false, body: '' })} type="button">Cancel</button>
      </div>
    )

  }

  render() {

    return (
      <div className="container comment-creator-wrapper">
        {this.state.creating ? this.renderCommentInput() : this.renderCreateButton()}
      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...mainActions }, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    errorRequestCategories: state.mainReducer.errorRequestCategories,
    loadingRequestCategories: state.mainReducer.loadingRequestCategories,
    categoriesData: state.mainReducer.categoriesData,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentCreator)