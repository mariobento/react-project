import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as mainActions from '../API/actions/mainActions'
import { Link } from 'react-router-dom'
import readingTime from 'reading-time'

class PostBuilder extends Component {

  constructor(props) {
    super(props)

    this.state = { likes: props.post.liked ? 'upVote' : undefined, likesCount: props.post.voteScore }
  }

  componentWillUnmount() {
    this.setState({ likes: undefined, likesCount: undefined })
  }

  getCategoryColor(catPath) {

    let color = 'red'

    if (!this.props.categoriesData) {
      return color
    }

    for (let key in this.props.categoriesData.categories) {
      let cat = this.props.categoriesData.categories[key]

      if (cat.path === catPath) {
        color = cat.color
      }

    }

    return color

  }

  clickedVote(post) {

    if (this.state.likes === 'upVote') {
      this.setState({ ...this.state, likes: 'downVote', likesCount: (this.state.likesCount - 1) }, () => {
        this.state.likes && this.props.actions.fetchRequestUpvotePost(post.id, 'downVote')
      })
    }

    if (this.state.likes === 'downVote' || !this.state.likes) {
      this.setState({ ...this.state, likes: 'upVote', likesCount: (this.state.likesCount + 1) }, () => {
        this.state.likes && this.props.actions.fetchRequestUpvotePost(post.id, 'upVote')
      })
    }

  }


  render() {

    let post = this.props.post

    if (!post) {
      return 'Loading ...'
    }

    let heartClass = this.state.likes === 'upVote' || post.liked ? 'fas fa-heart font-icon selected' : 'fas fa-heart font-icon'

    let text = (`${post.title} ${post.secondTitle} ${post.body} ${post.secondBody}`)

    // 100 words per minute is like a sloooooooow read; normal read would be arround 200
    let stats = readingTime(text, { wordsPerMinute: 100 })

    return (
      <article className="post-wrapper">
        <h2>{post.title}</h2>
        <div className="post-image">
          <img className='image' src={post.image} alt="post-graphic" />
        </div>
        <div className="post-second-title">
          <h3>{post.secondTitle}</h3>
        </div>
        <div className="loader-tag" style={{ color: this.getCategoryColor(post.category) }}>
          <p className='loader-cat-name'>{post.categoryName}</p>
          <p>
            <Link to={`/article/${post.id}`} style={{ color: this.getCategoryColor(post.category) }}>
              <i className="fas fa-comments font-icon">{` ${post.commentCount}`}</i>
            </Link>
            <span>
              <i onClick={() => this.clickedVote(post)} className={heartClass}>{` ${this.state.likesCount}`}</i>
            </span>
          </p>
          <p className='loader-cat-name'>{stats.text}</p>
        </div>
        <div className="post-body">
          <p>{post.body}</p>
        </div>
        <div className="shadow-div"></div>
        <Link to={`/article/${post.id}`} className='see-more-wrapper'>
          <div className="see-more">
            <p>Read more..</p>
          </div>
        </Link>
      </article>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...mainActions }, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    errorRequestCategories: state.mainReducer.errorRequestCategories,
    loadingRequestCategories: state.mainReducer.loadingRequestCategories,
    categoriesData: state.mainReducer.categoriesData,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostBuilder)