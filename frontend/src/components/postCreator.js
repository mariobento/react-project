import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as mainActions from '../API/actions/mainActions'
import Header from './header'

class PostCreator extends Component {

  constructor() {
    super()

    this.state = { title: '', secondTitle: '', body: '', secondBody: '', image: '', category: '', categoryName: '' }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChangeCategory = this.handleChangeCategory.bind(this)

  }

  handleChange(event, field) {
    this.setState({ ...this.state, [field]: event.target.value })
  }

  handleChangeCategory(e) {

    let catArr = this.props.categoriesData.categories.filter((o) => o.path === e.target.value)

    let cat = { name: 'all', path: 'all' }

    if (catArr.length > 0) {
      cat = catArr[0]
    }

    this.setState({ ...this.state, category: cat.path, categoryName: cat.name })

  }

  makeid() {
    var text = ""
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789"

    for (var i = 0; i < 10; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length))

    return text
  }

  handleSubmit() {

    let stateHolder = Object.assign({}, this.state)

    this.setState({ ...this.state, title: '', secondTitle: '', body: '', secondBody: '', image: '', category: '' }, () => {
      this.props.actions.fetchRequestAddPost(this.makeid(), Date.now(), stateHolder.title, stateHolder.secondTitle, stateHolder.body, stateHolder.secondBody, stateHolder.image, 'Mario Gaudencio', stateHolder.category, stateHolder.categoryName)
      this.props.history.push(`/${stateHolder.category}`)
    })

  }

  renderCategoriesOptions() {

    return this.props.categoriesData.categories.map(cat => {

      return (<option key={this.makeid()} value={cat.path}>{cat.name}</option>)

    })

  }

  renderPostInput() {

    return (
      <div className="form-wrapper">
        <div className="post-author-header">
          <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Cnn_logo_red_background.png/200px-Cnn_logo_red_background.png" alt="biavatar" />
          <p>Mario Gaudencio</p>
        </div>
        <label>
          Post Title:
          <textarea className='big-input' value={this.state.title} onChange={(e) => this.handleChange(e, 'title')} name="text-comment" id="text-comment" cols="12" rows="1"></textarea>
        </label>
        <label>
          Post Second Title:
        <textarea className='big-input' value={this.state.secondTitle} onChange={(e) => this.handleChange(e, 'secondTitle')} name="text-comment" id="text-comment" cols="12" rows="1"></textarea>
        </label>
        <label>
          Post Body:
          <textarea className='big-input' value={this.state.body} onChange={(e) => this.handleChange(e, 'body')} name="text-comment" id="text-comment" cols="12" rows="1"></textarea>
        </label>
        <label>
          Post Second Body:
          <textarea className='big-input' value={this.state.secondBody} onChange={(e) => this.handleChange(e, 'secondBody')} name="text-comment" id="text-comment" cols="12" rows="1"></textarea>
        </label>
        <label>
          Post Image Url:
          <textarea value={this.state.image} onChange={(e) => this.handleChange(e, 'image')} name="text-comment" id="text-comment" cols="12" rows="1"></textarea>
        </label>
        {this.props.categoriesData &&
          <label>
            Category:
            <select value={this.state.category} onChange={this.handleChangeCategory}>
              {this.renderCategoriesOptions()}
            </select>
          </label>}
        <button onClick={() => this.handleSubmit()} type="button">Submit Post</button>
      </div>
    )

  }

  render() {

    return (
      <div className="post-creator-wrapper">
        <Header match={this.props.match} history={this.props.history} />
        <div className="container creator">
          {this.renderPostInput()}
        </div>
      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...mainActions }, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    errorRequestCategories: state.mainReducer.errorRequestCategories,
    loadingRequestCategories: state.mainReducer.loadingRequestCategories,
    categoriesData: state.mainReducer.categoriesData,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostCreator)