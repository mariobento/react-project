import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as mainActions from '../API/actions/mainActions'
import Header from './header'

class PostBuilder extends Component {

  constructor(props) {
    super(props)

    this.state = { title: '', secondTitle: '', body: '', secondBody: '', timestamp: Date.now() }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.match.params.whatToEdit === 'post-editor' && nextProps.postDetail) {
      this.setState({ ...this.state, title: nextProps.postDetail.title, secondTitle: nextProps.postDetail.secondTitle, body: nextProps.postDetail.body, secondBody: nextProps.postDetail.secondBody })
    } else if (nextProps.match.params.whatToEdit === 'comment-editor' && nextProps.commentDetail) {
      this.setState({ ...this.state, timestamp: nextProps.commentDetail.timestamp, body: nextProps.commentDetail.body })
    }

  }

  componentWillMount() {

    if (this.props.match.params.whatToEdit === 'post-editor') {
      this.props.actions.fetchRequestPostDetails(this.props.match.params.whatToEditId)
    } else {
      this.props.actions.fetchRequestCommentDetails(this.props.match.params.whatToEditId)
    }

  }

  handleChange(event, field) {
    this.setState({ ...this.state, [field]: event.target.value })
  }


  handleSubmit(event) {
    event.preventDefault()

    if (this.props.match.params.whatToEdit === 'post-editor') {
      this.props.actions.fetchRequestEditPost(this.props.postDetail.id, this.state.title, this.state.secondTitle, this.state.body, this.state.secondBody)
      this.props.history.push(`/article/${this.props.postDetail.id}`)
    } else {
      this.props.actions.fetchRequestEditComment(this.props.commentDetail.id, this.props.commentDetail.timestamp, this.state.body)
      this.props.history.push(`/article/${this.props.commentDetail.parentId}`)
    }

  }

  render() {
    return (
      <div className="editor">
        <Header match={this.props.match} history={this.props.history} />
        <div className="container editor-wrapper">
          <h1>{this.props.match.params.whatToEdit === 'post-editor' ? 'Post Editor' : 'Comment Editor'}</h1>
          {this.props.match.params.whatToEdit === 'post-editor' &&
            <form onSubmit={(e) => this.handleSubmit(e, 'post')}>
              <label>
                Title:
              <textarea className='big-input' value={this.state.title} onChange={(e) => this.handleChange(e, 'title')} name="text-comment" id="text-comment" cols="12" rows="1"></textarea>
              </label>
              <label>
                Second Title:
            <textarea className='big-input' value={this.state.secondTitle} onChange={(e) => this.handleChange(e, 'secondTitle')} name="text-comment" id="text-comment" cols="12" rows="1"></textarea>
              </label>
              <label>
                Body:
                <textarea className='big-input' value={this.state.body} onChange={(e) => this.handleChange(e, 'body')} name="text-comment" id="text-comment" cols="12" rows="1"></textarea>
              </label>
              <label>
                Second Body:
                <textarea className='big-input' value={this.state.secondBody} onChange={(e) => this.handleChange(e, 'secondBody')} name="text-comment" id="text-comment" cols="12" rows="1"></textarea>
              </label>
              <input type="submit" value="Submit" />
            </form>}
          {this.props.match.params.whatToEdit === 'comment-editor' &&
            <form onSubmit={(e) => this.handleSubmit(e, 'post')}>
              <label>
                Body:
                <textarea className='big-input' value={this.state.body} onChange={(e) => this.handleChange(e, 'body')} name="text-comment" id="text-comment" cols="12" rows="1"></textarea>
              </label>
              <input type="submit" value="Submit" />
            </form>}
        </div>
      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...mainActions }, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    errorRequestPostDetails: state.mainReducer.errorRequestPostDetails,
    loadingRequestPostDetails: state.mainReducer.loadingRequestPostDetails,
    postDetail: state.mainReducer.postDetail,

    errorRequestCommentDetails: state.mainReducer.errorRequestCommentDetails,
    loadingRequestCommentDetails: state.mainReducer.loadingRequestCommentDetails,
    commentDetail: state.mainReducer.commentDetail,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostBuilder)