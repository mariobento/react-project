import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as mainActions from '../API/actions/mainActions'
import PostBuilder from './postBuilder'

let catReference

class ContentLoader extends Component {

  constructor(props) {
    super(props)

    let cat = this.getActiveCat(props)

    this.state = { activeCat: cat }

  }

  componentWillMount() {

    this.getPosts(this.state.activeCat)

  }

  getActiveCat(props) {

    return (props.match.params.category ? props.match.params.category : 'all')

  }

  getPosts(cat) {

    if (cat === 'all') {
      this.props.actions.fetchRequestAllPosts()
    } else {
      this.props.actions.fetchRequestPostsFromCategory(this.state.activeCat)
    }

  }

  componentWillReceiveProps(nextProps) {

    let cat = this.getActiveCat(nextProps)

    if (cat !== catReference) {
      catReference = cat
      this.setState({ ...this.state, activeCat: cat }, () => {
        this.getPosts(this.state.activeCat)
      })
    }

  }

  renderPosts(obj) {

    return obj.map((post, idx) => {
      return (<PostBuilder key={idx} post={post} idx={idx} />)
    })

  }

  render() {

    let obj = this.props.postsContent

    return (
      <section className="content-loader-wrapper">
        <div className="create-post-wrapper">
          <p onClick={() => this.props.history.push('/post/create')}>Create Post</p>
        </div>
        {obj && obj.length > 0 && this.renderPosts(obj)}
      </section>
    )
  }

}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...mainActions }, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    categoriesData: state.mainReducer.categoriesData,

    errorRequestPostsCategory: state.mainReducer.errorRequestPostsCategory,
    loadingRequestPostsCategory: state.mainReducer.loadingRequestPostsCategory,
    postsContent: state.mainReducer.postsContent,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContentLoader)