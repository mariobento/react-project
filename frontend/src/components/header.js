import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as mainActions from '../API/actions/mainActions'
import $ from 'jquery'
import { Link } from 'react-router-dom'

class Header extends Component {

  componentWillMount() {

    this.props.actions.fetchRequestAllCategories()

  }

  clickedCategory(catPath) {

    $("html, body").animate({ scrollTop: 0 }, "slow")
    this.props.history.push(`/${catPath}`)

  }

  renderCategoriesTags() {

    if (this.props.categoriesData) {

      let selectedCat = this.props.match.params.category ? this.props.match.params.category : 'all'

      let obj = this.props.categoriesData.categories

      return obj.map((tag, idx) => {
        let tagClass = selectedCat === tag.path ? 'tag selected' : 'tag'
        return (
          <div key={idx} onClick={() => this.clickedCategory(tag.path)} className={tagClass} style={{ border: `1px solid ${tag.color}`, color: tag.color }}>
            <p>{tag.name}</p>
          </div>
        )
      })

    }

    return 'Loading categories...'

  }

  render() {

    return (
      <nav className="header-wrapper">
        <Link className='header-title-link' to='/all'>
          <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Cnn_logo_red_background.png/200px-Cnn_logo_red_background.png" alt="biheader" />
        </Link>
        {!this.props.match.params.whatToEdit && !this.props.match.params.create &&
          <div className="tag-wrapper">
            {this.renderCategoriesTags()}
          </div>}
      </nav>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...mainActions }, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    errorRequestCategories: state.mainReducer.errorRequestCategories,
    loadingRequestCategories: state.mainReducer.loadingRequestCategories,
    categoriesData: state.mainReducer.categoriesData,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)