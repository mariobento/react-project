import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as mainActions from '../API/actions/mainActions'
import readingTime from 'reading-time'
import CommentBuilder from './commentBuilder'
import CommentCreator from './commentCreator'
import Moment from 'react-moment'
import $ from 'jquery'

class ArticleLoader extends Component {

  constructor(props) {
    super(props)

    this.state = { contained: false, activePost: props.match.params.postId }

  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.postDetail && !nextProps.postComments) {
      this.props.actions.fetchRequestPostComments(nextProps.postDetail.id)
    }

  }

  componentWillUnmount() {

    this.props.actions.requestClearCommentsAction()

  }

  componentWillMount() {

    let postId = this.props.match.params.postId
    $("html, body").animate({ scrollTop: 0 }, "slow")

    if (postId) {
      this.props.actions.fetchRequestPostDetails(postId)
    }

  }

  renderComments() {

    return this.props.postComments.map((comment, idx) => {
      return (<CommentBuilder history={this.props.history} key={idx} comment={comment} idx={idx} />)
    })

  }

  clickedEdit(postId) {

    this.props.history.push(`/edit/post-editor/${postId}`)

  }

  clickedDelete(post) {

    this.props.actions.fetchRequestDeletePost(post.id)
    this.props.history.push(`/${post.category}`)

  }

  render() {

    if (this.props.postDetail) {

      let text = (`${this.props.postDetail.title} ${this.props.postDetail.secondTitle} ${this.props.postDetail.body} ${this.props.postDetail.secondBody}`)

      // 100 words per minute is like a sloooooooow read; normal read would be arround 200
      let stats = readingTime(text, { wordsPerMinute: 100 })

      let classImage = this.state.contained ? 'cover-background contained' : 'cover-background'

      return (
        <div className="article-loader-wrapper">
          <div onClick={() => this.setState({ ...this.state, contained: !this.state.contained })} className={classImage} style={{ backgroundImage: `url(${this.props.postDetail.image})` }}>
            <div className="warner">
              <p className='pulsate'>Click to view full image</p>
            </div>
          </div>
          <div className="container author-header">
            <img className='author-avatar' src={this.props.postDetail.avatar} alt="author-avatar" />
            <div className="author-name">
              <p className='label'>AUTHOR</p>
              <p>{this.props.postDetail.author}</p>
            </div>
            <div className="article-header-extra-info">
              <Moment unix format="DD-MM-YYYY HH:mm">{this.props.postDetail.timestamp}</Moment>
              <p>{stats.text}</p>
            </div>
            {this.props.postDetail.owned &&
              <div className="post-actions">
                <p onClick={() => this.clickedEdit(this.props.postDetail.id)}>Edit Post</p>
                <p onClick={() => this.clickedDelete(this.props.postDetail)}>Delete post</p>
              </div>}
          </div>
          <div className='container article-first-title'>{this.props.postDetail.title}</div>
          <div className='container article-second-title'>{this.props.postDetail.secondTitle}</div>
          <div className='container article-body'>{this.props.postDetail.body}</div>
          <div className='container article-second-body'>{this.props.postDetail.secondBody}</div>
          {this.props.postComments &&
            <div className="container article-comments-wrapper">{this.renderComments()}</div>}
          <CommentCreator post={this.props.postDetail} />
        </div>
      )

    } else {
      return (<p>Loading post info...</p>)
    }

  }

}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...mainActions }, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    categoriesData: state.mainReducer.categoriesData,

    errorRequestPostDetails: state.mainReducer.errorRequestPostDetails,
    loadingRequestPostDetails: state.mainReducer.loadingRequestPostDetails,
    postDetail: state.mainReducer.postDetail,

    errorRequestPostComments: state.mainReducer.errorRequestPostComments,
    loadingRequestPostComments: state.mainReducer.loadingRequestPostComments,
    postComments: state.mainReducer.postComments,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleLoader)