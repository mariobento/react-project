import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as mainActions from '../API/actions/mainActions'
import Moment from 'react-moment'

class CommentBuilder extends Component {

  constructor(props) {
    super(props)

    this.state = { likes: props.comment.liked ? 'upVote' : undefined, likesCount: props.comment.voteScore }
  }

  clickedVote(comment) {

    if (this.state.likes === 'upVote') {
      this.setState({ ...this.state, likes: 'downVote', likesCount: (this.state.likesCount - 1) }, () => {
        this.state.likes && this.props.actions.fetchRequestUpvoteComment(comment.id, 'downVote')
      })
    }

    if (this.state.likes === 'downVote' || !this.state.likes) {
      this.setState({ ...this.state, likes: 'upVote', likesCount: (this.state.likesCount + 1) }, () => {
        this.state.likes && this.props.actions.fetchRequestUpvoteComment(comment.id, 'upVote')
      })
    }

  }

  deleteComment(comment) {

    this.props.actions.fetchRequestDeleteComment(comment.id)
    this.props.actions.fetchRequestPostComments(comment.parentId)

  }

  render() {

    let comment = this.props.comment

    let heartClass = this.state.likes === 'upVote' || comment.liked ? 'fas fa-heart font-icon selected' : 'fas fa-heart font-icon'

    return (
      <div className="comment-wrapper">
        <div className="comment-header">
          <div className="author">
            <img src={comment.avatar} alt="commentauthoravatar" />
            <div className="author-name">
              <p>{comment.author}</p>
              <Moment unix format="YYYY-MM-DD HH:mm">{comment.timestamp}</Moment>
            </div>
            <div className="comment-actions">
              <i onClick={() => this.clickedVote(comment)} className={heartClass}>{` ${this.state.likesCount}`}</i>
              {comment.owned &&
                <i onClick={() => this.props.history.push(`/edit/comment-editor/${comment.id}`)} className='fas fa-edit'></i>}
              {comment.owned &&
                <i onClick={() => this.deleteComment(comment)} className='fas fa-trash-alt'></i>}
            </div>
          </div>
        </div>
        <div className="container comment-body">
          <p>{comment.body}</p>
        </div>
      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...mainActions }, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    errorRequestCategories: state.mainReducer.errorRequestCategories,
    loadingRequestCategories: state.mainReducer.loadingRequestCategories,
    categoriesData: state.mainReducer.categoriesData,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentBuilder)