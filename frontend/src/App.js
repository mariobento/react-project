import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as mainActions from './API/actions/mainActions'
import Header from './components/header'
import ContentLoader from './components/contentLoader'
import ArticleLoader from './components/articleLoader'
import _ from 'lodash'

class App extends Component {

  componentWillMount() {

    if (_.isEmpty(this.props.match.params)) {
      this.props.history.push('/all')
    }

  }

  render() {

    return (
      <div className="App">
        <Header match={this.props.match} history={this.props.history} />
        {this.props.match.params.postId ?
          <ArticleLoader match={this.props.match} history={this.props.history} /> :
          <div className="main-wrapper">
            <ContentLoader match={this.props.match} history={this.props.history} />
          </div>}

      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...mainActions }, dispatch)
  }
}

function mapStateToProps(state) {
  return {
    BI: state.mainReducer.BI,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)