import { combineReducers } from 'redux'
import mainReducer from '../API/reducers/mainReducer'

const rootReducer = combineReducers({
  mainReducer,
})

export default rootReducer
