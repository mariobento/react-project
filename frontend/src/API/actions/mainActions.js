import * as types from './actionTypes'
import mainApi from '../apiCalls/mainCalls'

/*
  * [GET]
*/

export function errorRequestCategories(error) {
  return {
    type: types.ERROR_REQUEST_ALL_CATEGORIES,
    error: 'Something went wrong, try again later!'
  }
}

export function successRequestCategories(response) {
  return {
    type: types.SUCCESS_REQUEST_ALL_CATEGORIES,
    response: response
  }
}

export function requestCategories() {
  return {
    type: types.REQUEST_ALL_CATEGORIES,
  }
}

export function fetchRequestAllCategories() {
  return dispatch => {
    dispatch(requestCategories())
    return mainApi.requestAllCategories().then(response => {
      dispatch(successRequestCategories(response));
    }).catch(error => {
      dispatch(errorRequestCategories(error));
    })

  }
}

export function errorRequestAllPosts(error) {
  return {
    type: types.ERROR_REQUEST_ALL_POSTS,
    error: 'Something went wrong, try again later!'
  }
}

export function successRequestAllPosts(response) {
  return {
    type: types.SUCCESS_REQUEST_ALL_POSTS,
    response: response
  }
}

export function requestAllPosts() {
  return {
    type: types.REQUEST_ALL_POSTS,
  }
}

export function fetchRequestAllPosts() {
  return dispatch => {
    dispatch(requestAllPosts())
    return mainApi.requestAllPosts().then(response => {
      dispatch(successRequestAllPosts(response));
    }).catch(error => {
      dispatch(errorRequestAllPosts(error));
    })

  }
}

export function errorRequestPostsFromCategory(error) {
  return {
    type: types.ERROR_REQUEST_POSTS_WITH_CATEGORY,
    error: 'Something went wrong, try again later!'
  }
}

export function successRequestPostsFromCategory(response) {
  return {
    type: types.SUCCESS_REQUEST_POSTS_WITH_CATEGORIES,
    response: response
  }
}

export function requestPostsFromCategory() {
  return {
    type: types.REQUEST_POSTS_WITH_CATEGORIES,
  }
}

export function fetchRequestPostsFromCategory(category) {
  return dispatch => {
    dispatch(requestPostsFromCategory())
    return mainApi.requestAllPostsFromCategory(category).then(response => {
      dispatch(successRequestPostsFromCategory(response));
    }).catch(error => {
      dispatch(errorRequestPostsFromCategory(error));
    })

  }
}

export function errorRequestPostDetails(error) {
  return {
    type: types.ERROR_REQUEST_POST_DETAILS,
    error: 'Something went wrong, try again later!'
  }
}

export function successRequestPostDetails(response) {
  return {
    type: types.SUCCESS_REQUEST_POSTS_DETAILS,
    response: response
  }
}

export function requestPostDetails() {
  return {
    type: types.REQUEST_POSTS_DETAILS,
  }
}

export function fetchRequestPostDetails(postId) {
  return dispatch => {
    dispatch(requestPostDetails())
    return mainApi.requestPostDetails(postId).then(response => {
      dispatch(successRequestPostDetails(response));
    }).catch(error => {
      dispatch(errorRequestPostDetails(error));
    })

  }
}

export function errorRequestPostComments(error) {
  return {
    type: types.ERROR_REQUEST_POST_COMMENTS,
    error: 'Something went wrong, try again later!'
  }
}

export function successRequestPostComments(response) {
  return {
    type: types.SUCCESS_REQUEST_POSTS_COMMENTS,
    response: response
  }
}

export function requestPostComments() {
  return {
    type: types.REQUEST_POSTS_COMMENTS,
  }
}

export function fetchRequestPostComments(postId) {
  return dispatch => {
    dispatch(requestPostComments())
    return mainApi.requestPostComments(postId).then(response => {
      dispatch(successRequestPostComments(response));
    }).catch(error => {
      dispatch(errorRequestPostComments(error));
    })

  }
}

export function errorRequestCommentDetails(error) {
  return {
    type: types.ERROR_REQUEST_COMMENT_DETAILS,
    error: 'Something went wrong, try again later!'
  }
}

export function successRequestCommentDetails(response) {
  return {
    type: types.SUCCESS_REQUEST_COMMENT_DETAILS,
    response: response
  }
}

export function requestCommentDetails() {
  return {
    type: types.REQUEST_COMMENT_DETAILS,
  }
}

export function fetchRequestCommentDetails(commentId) {
  return dispatch => {
    dispatch(requestCommentDetails())
    return mainApi.requestCommentDetails(commentId).then(response => {
      dispatch(successRequestCommentDetails(response));
    }).catch(error => {
      dispatch(errorRequestCommentDetails(error));
    })

  }
}

/*
  * [POST]
*/

export function errorRequestAddingPost(error) {
  return {
    type: types.ERROR_ADDING_POST,
    error: 'Something went wrong, try again later!'
  }
}

export function successRequestAddingPost(response) {
  return {
    type: types.SUCCESS_ADDING_POST,
    response: response
  }
}

export function requestAddingPost() {
  return {
    type: types.REQUEST_ADDING_POST,
  }
}

export function fetchRequestAddPost(postId, timestamp, title, secondTitle, body, secondBody, image, author, category, categoryName) {
  return dispatch => {
    dispatch(requestCommentDetails())
    return mainApi.requestAddPost(postId, timestamp, title, secondTitle, body, secondBody, image, author, category, categoryName).then(response => {
      dispatch(successRequestCommentDetails(response));
    }).catch(error => {
      dispatch(errorRequestCommentDetails(error));
    })

  }
}

export function errorRequestUpvotePost(error) {
  return {
    type: types.ERROR_UPVOTE_POST,
    error: 'Something went wrong, try again later!'
  }
}

export function successRequestUpvotePost(response) {
  return {
    type: types.SUCCESS_UPVOTE_POST,
    response: response
  }
}

export function requestUpvotePost() {
  return {
    type: types.REQUEST_UPVOTE_POST,
  }
}

export function fetchRequestUpvotePost(postId, status) {
  return dispatch => {
    dispatch(requestUpvotePost())
    return mainApi.requestUpvotePost(postId, status).then(response => {
      dispatch(successRequestUpvotePost(response));
    }).catch(error => {
      dispatch(errorRequestUpvotePost(error));
    })

  }
}

export function errorRequestaddCommentToPost(error) {
  return {
    type: types.ERROR_ADDING_COMMENT,
    error: 'Something went wrong, try again later!'
  }
}

export function successRequestAddCommentToPost(response) {
  return {
    type: types.SUCCESS_ADDING_COMMENT,
    response: response
  }
}

export function requestAddCommentToPost() {
  return {
    type: types.REQUEST_ADDING_COMMENT,
  }
}

export function fetchRequestAddCommentToPost(commentId, timestamp, body, author, parentId) {
  return dispatch => {
    dispatch(requestAddCommentToPost())
    return mainApi.requestAddCommentToPost(commentId, timestamp, body, author, parentId).then(response => {
      dispatch(successRequestAddCommentToPost(response));
    }).catch(error => {
      dispatch(errorRequestaddCommentToPost(error));
    })

  }
}

export function errorRequestUpvoteComment(error) {
  return {
    type: types.ERROR_UPVOTE_COMMENT,
    error: 'Something went wrong, try again later!'
  }
}

export function successRequestUpvoteComment(response) {
  return {
    type: types.SUCCESS_UPVOTE_COMMENT,
    response: response
  }
}

export function requestUpvoteComment() {
  return {
    type: types.REQUEST_UPVOTE_COMMENT,
  }
}

export function fetchRequestUpvoteComment(commentId, status) {
  return dispatch => {
    dispatch(requestUpvoteComment())
    return mainApi.requestUpvoteComment(commentId, status).then(response => {
      dispatch(successRequestUpvoteComment(response));
    }).catch(error => {
      dispatch(errorRequestUpvoteComment(error));
    })

  }
}

/*
  * [PUT]
*/

export function errorRequestEditPost(error) {
  return {
    type: types.ERROR_EDIT_POST,
    error: 'Something went wrong, try again later!'
  }
}

export function successRequestEditPost(response) {
  return {
    type: types.SUCCESS_EDIT_POST,
    response: response
  }
}

export function requestEditPost() {
  return {
    type: types.REQUEST_EDIT_POST,
  }
}

export function fetchRequestEditPost(postId, title, secondTitle, body, secondBody) {
  return dispatch => {
    dispatch(requestEditPost())
    return mainApi.requestEditPost(postId, title, secondTitle, body, secondBody).then(response => {
      dispatch(successRequestEditPost(response));
    }).catch(error => {
      dispatch(errorRequestEditPost(error));
    })

  }
}

export function errorRequestEditComment(error) {
  return {
    type: types.ERROR_EDIT_COMMENT,
    error: 'Something went wrong, try again later!'
  }
}

export function successRequestEditComment(response) {
  return {
    type: types.SUCCESS_EDIT_COMMENT,
    response: response
  }
}

export function requestEditComment() {
  return {
    type: types.REQUEST_EDIT_COMMENT,
  }
}

export function fetchRequestEditComment(commentId, timestamp, body) {
  return dispatch => {
    dispatch(requestEditComment())
    return mainApi.requestEditComment(commentId, timestamp, body).then(response => {
      dispatch(successRequestEditComment(response));
    }).catch(error => {
      dispatch(errorRequestEditComment(error));
    })

  }
}

export function errorRequestDeletePost(error) {
  return {
    type: types.ERROR_DELETE_POST,
    error: 'Something went wrong, try again later!'
  }
}

export function successRequestDeletePost(response) {
  return {
    type: types.SUCCESS_DELETE_POST,
    response: response
  }
}

export function requestDeletePost() {
  return {
    type: types.REQUEST_DELETE_POST,
  }
}

export function fetchRequestDeletePost(postId) {
  return dispatch => {
    dispatch(requestDeletePost())
    return mainApi.requestDeletePost(postId).then(response => {
      dispatch(successRequestDeletePost(response));
    }).catch(error => {
      dispatch(errorRequestDeletePost(error));
    })

  }
}

export function errorRequestDeleteComment(error) {
  return {
    type: types.ERROR_DELETE_COMMENT,
    error: 'Something went wrong, try again later!'
  }
}

export function successRequestDeleteComment(response) {
  return {
    type: types.SUCCESS_DELETE_COMMENT,
    response: response
  }
}

export function requestDeleteComment() {
  return {
    type: types.REQUEST_DELETE_COMMENT,
  }
}

export function fetchRequestDeleteComment(commentId) {
  return dispatch => {
    dispatch(requestDeleteComment())
    return mainApi.requestDeleteComment(commentId).then(response => {
      dispatch(successRequestDeleteComment(response));
    }).catch(error => {
      dispatch(errorRequestDeleteComment(error));
    })

  }
}

export function requestClearCommentsAction() {
  return {
    type: types.CLEAR_COMMENTS_ACTION,
  }
}



