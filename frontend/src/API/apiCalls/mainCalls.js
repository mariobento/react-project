import apiEnv from '../apiEnv'

class MainCalls {

  /*
    * [GET] 
   */

  static requestAllCategories() {
    const request = new Request(`${apiEnv()}categories`, {
      method: 'GET',
      headers: new Headers({
        'Authorization': 'HEY THERE',
      })
    })
    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      throw (error)
    })
  }

  static requestAllPosts() {
    const request = new Request(`${apiEnv()}posts`, {
      method: 'GET',
      headers: new Headers({
        'Authorization': 'HEY THERE',
      })
    })
    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      throw (error)
    })
  }

  static requestAllPostsFromCategory(category) {
    const request = new Request(`${apiEnv()}${category}/posts`, {
      method: 'GET',
      headers: new Headers({
        'Authorization': 'HEY THERE',
      })
    })
    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      throw (error)
    })
  }

  static requestPostDetails(postId) {
    const request = new Request(`${apiEnv()}posts/${postId}`, {
      method: 'GET',
      headers: new Headers({
        'Authorization': 'HEY THERE',
      })
    })
    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      throw (error)
    })
  }

  static requestPostComments(postId) {
    const request = new Request(`${apiEnv()}posts/${postId}/comments`, {
      method: 'GET',
      headers: new Headers({
        'Authorization': 'HEY THERE',
      })
    })
    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      throw (error)
    })
  }

  static requestCommentDetails(commentId) {
    const request = new Request(`${apiEnv()}comments/${commentId}`, {
      method: 'GET',
      headers: new Headers({
        'Authorization': 'HEY THERE',
      })
    })
    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      throw (error)
    })
  }

  /*
    * [POST] 
   */

  static requestAddPost(postId, timestamp, title, secondTitle, body, secondBody, image, author, category, categoryName) {
    let bodyObj = { id: postId, timestamp, title, secondTitle, body, secondBody, image, author, category, categoryName }
    const request = new Request(`${apiEnv()}posts`, {
      method: 'POST',
      headers: new Headers({
        'Authorization': 'HEY THERE',
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(bodyObj)
    })
    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      throw (error)
    })
  }

  static requestUpvotePost(postId, status) {
    let bodyObj = { option: status }
    const request = new Request(`${apiEnv()}posts/${postId}?option=${status}`, {
      method: 'POST',
      headers: new Headers({
        'Authorization': 'HEY THERE',
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(bodyObj)
    })
    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      throw (error)
    })
  }

  static requestAddCommentToPost(commentId, timestamp, body, author, parentId) {
    let bodyObj = { id: commentId, timestamp, body, author, parentId }
    const request = new Request(`${apiEnv()}comments`, {
      method: 'POST',
      headers: new Headers({
        'Authorization': 'HEY THERE',
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(bodyObj)
    })
    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      throw (error)
    })
  }

  static requestUpvoteComment(commentId, status) {
    let bodyObj = { option: status }
    const request = new Request(`${apiEnv()}comments/${commentId}`, {
      method: 'POST',
      headers: new Headers({
        'Authorization': 'HEY THERE',
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(bodyObj)
    })
    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      throw (error)
    })
  }

  /*
    * [PUT]
  */

  static requestEditPost(postId, title, secondTitle, body, secondBody) {
    let bodyObj = { title, secondTitle, body, secondBody }
    const request = new Request(`${apiEnv()}posts/${postId}`, {
      method: 'PUT',
      headers: new Headers({
        'Authorization': 'HEY THERE',
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(bodyObj)
    })
    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      throw (error)
    })
  }

  static requestEditComment(commentId, timestamp, body) {
    let bodyObj = { timestamp, body }
    const request = new Request(`${apiEnv()}comments/${commentId}`, {
      method: 'PUT',
      headers: new Headers({
        'Authorization': 'HEY THERE',
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(bodyObj)
    })
    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      throw (error)
    })
  }

  /*
    * [DELETE] 
   */

  static requestDeletePost(postId) {
    const request = new Request(`${apiEnv()}posts/${postId}`, {
      method: 'DELETE',
      headers: new Headers({
        'Authorization': 'HEY THERE',
      })
    })
    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      throw (error)
    })
  }

  static requestDeleteComment(commentId) {
    const request = new Request(`${apiEnv()}comments/${commentId}`, {
      method: 'DELETE',
      headers: new Headers({
        'Authorization': 'HEY THERE',
      })
    })
    return fetch(request).then(response => {
      return response.json()
    }).catch(error => {
      throw (error)
    })
  }

}

export default MainCalls