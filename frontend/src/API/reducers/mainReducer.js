import * as types from '../actions/actionTypes'
import initialState from './initialState'

export default function mainReducer(state = initialState.mainReducer, action) {
  switch (action.type) {
    case types.DEFAULT_TESTE_ACTION:
      return {
        ...state,
        BI: 'Hello BI',
      }
    case types.ERROR_REQUEST_ALL_CATEGORIES:
      return {
        ...state,
        errorRequestCategories: action.error,
        loadingRequestCategories: false,
        categoriesData: state.categoriesData,
      }
    case types.SUCCESS_REQUEST_ALL_CATEGORIES:
      return {
        ...state,
        errorRequestCategories: false,
        loadingRequestCategories: false,
        categoriesData: action.response,
      }
    case types.REQUEST_ALL_CATEGORIES:
      return {
        ...state,
        errorRequestCategories: false,
        loadingRequestCategories: true,
        categoriesData: state.categoriesData,
      }
    case types.ERROR_REQUEST_POSTS_WITH_CATEGORY:
      return {
        ...state,
        errorRequestPostsCategory: action.error,
        loadingRequestPostsCategory: false,
        postsContent: undefined,
      }
    case types.SUCCESS_REQUEST_POSTS_WITH_CATEGORIES:
      return {
        ...state,
        errorRequestPostsCategory: false,
        loadingRequestPostsCategory: false,
        postsContent: action.response.reverse(),
      }
    case types.REQUEST_POSTS_WITH_CATEGORIES:
      return {
        ...state,
        errorRequestPostsCategory: false,
        loadingRequestPostsCategory: false,
        postsContent: undefined,
      }
    case types.ERROR_REQUEST_ALL_POSTS:
      return {
        ...state,
        errorRequestPosts: action.error,
        loadingRequestPosts: false,
        postsContent: undefined,
      }
    case types.SUCCESS_REQUEST_ALL_POSTS:
      return {
        ...state,
        errorRequestPosts: false,
        loadingRequestPosts: false,
        postsContent: action.response.reverse(),
      }
    case types.REQUEST_ALL_POSTS:
      return {
        ...state,
        errorRequestPosts: false,
        loadingRequestPosts: false,
        postsContent: undefined,
      }
    case types.ERROR_REQUEST_POST_DETAILS:
      return {
        ...state,
        errorRequestPostDetails: action.error,
        loadingRequestPostDetails: false,
        postDetail: state.postDetail,
      }
    case types.SUCCESS_REQUEST_POSTS_DETAILS:
      return {
        ...state,
        errorRequestPostDetails: false,
        loadingRequestPostDetails: false,
        postDetail: action.response,
      }
    case types.REQUEST_POSTS_DETAILS:
      return {
        ...state,
        errorRequestPostDetails: false,
        loadingRequestPostDetails: false,
        postDetail: state.postDetail,
      }
    case types.ERROR_REQUEST_COMMENT_DETAILS:
      return {
        ...state,
        errorRequestCommentDetails: action.error,
        loadingRequestCommentDetails: false,
        commentDetail: state.commentDetail,
      }
    case types.SUCCESS_REQUEST_COMMENT_DETAILS:
      return {
        ...state,
        errorRequestCommentDetails: false,
        loadingRequestCommentDetails: false,
        commentDetail: action.response,
      }
    case types.REQUEST_COMMENT_DETAILS:
      return {
        ...state,
        errorRequestCommentDetails: false,
        loadingRequestCommentDetails: false,
        commentDetail: state.commentDetail,
      }
    case types.ERROR_REQUEST_POST_COMMENTS:
      return {
        ...state,
        errorRequestPostComments: action.error,
        loadingRequestPostComments: false,
        postComments: undefined,
      }
    case types.SUCCESS_REQUEST_POSTS_COMMENTS:
      return {
        ...state,
        errorRequestPostComments: false,
        loadingRequestPostComments: false,
        postComments: action.response.reverse(),
      }
    case types.REQUEST_POSTS_COMMENTS:
      return {
        ...state,
        errorRequestPostComments: false,
        loadingRequestPostComments: false,
        postComments: undefined,
      }
    case types.CLEAR_COMMENTS_ACTION:
      return {
        ...state,
        errorRequestPostComments: false,
        loadingRequestPostComments: false,
        postComments: undefined,
      }
    default:
      return state
  }
}
